# Copyright 2013-2019 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
#
#     spack install py-sen1-ard-gamma
#
# You can edit this file again by typing:
#
#     spack edit py-sen1-ard-gamma
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PySen1ArdGamma(PythonPackage):
    """Software for processing ESA Sentinel-1 data to generate ARD products using Gamma Software."""

    homepage = "https://bitbucket.org/petebunting/sen1_ard_gamma"
    url      = "https://bitbucket.org/petebunting/sen1_ard_gamma/downloads/sen1_ard_gamma-0.1.7.tar.gz"

    version('0.1.7', sha256='bb10a5ace5e792bdea5674319ed3e951b8374b87e9d8567122e093dead3f27d7')

    depends_on('gdal', type=('build', 'run'))
    depends_on('python', type=('build', 'run'))



